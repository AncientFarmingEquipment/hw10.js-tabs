const tabs = document.querySelector('.tabs');
tabs.addEventListener('click', (e) => {
    let previousTab = document.querySelector('.tabs .active');
    if (e.target !== e.currentTarget && e.target.classList.contains('tabs-title')) {
        previousTab.classList.remove('active');
        document.querySelector('.tabs-content .show').classList.remove('show');
        e.target.classList.add('active');
        document.querySelector(`[data-text="${e.target.innerText.toLowerCase()}"]`).classList.add('show');
    }
})
